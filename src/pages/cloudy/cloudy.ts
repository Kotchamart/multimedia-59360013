import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NamePage} from '../name/name';
import { DegreePage } from '../degree/degree';
import { TypePage } from '../type/type';

/**
 * Generated class for the CloudyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cloudy',
  templateUrl: 'cloudy.html',
})
export class CloudyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CloudyPage');
  }
  name(){
    this.navCtrl.push(NamePage);
  }
  type(){
    this.navCtrl.push(TypePage);
  }
  degree(){
    this.navCtrl.push(DegreePage);
  }

}
