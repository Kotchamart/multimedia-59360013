import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloudyPage } from './cloudy';

@NgModule({
  declarations: [
    CloudyPage,
  ],
  imports: [
    IonicPageModule.forChild(CloudyPage),
  ],
})
export class CloudyPageModule {}
