import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DegreePage } from './degree';

@NgModule({
  declarations: [
    DegreePage,
  ],
  imports: [
    IonicPageModule.forChild(DegreePage),
  ],
})
export class DegreePageModule {}
