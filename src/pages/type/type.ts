import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type',
  templateUrl: 'type.html',
})
export class TypePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TypePage');
  }
  slider = [
    {
      title : '" ก า ร เ รี ย ก ชื่ อ เ ม ฆ "',
      description : 'การเรียกชื่อเมฆจะเรียกตามระดับความสูงและลักษณะรูปร่างของก้อนเมฆ ซึ่งเมฆสามารถแบ่งตามระดับความสูง (Altitude) ของฐานเมฆ (Cloud Base) ได้ออกเป็น 3 กลุ่มใหญ่ๆ' ,
      image : "assets/imgs/cauliflower.svg",
      image1 : "assets/imgs/cloud2.png"
    },
    {
      title : ' เมฆระดับสูง เซอร์โร(Cirro)',
      description : 'จะอยู่ที่ความสูง 6,000 เมตร ขึ้นไป จึงทำให้เห็นเมฆชนิดนี้มีขนาดเล็กกว่าเมฆชนิดอื่นๆ ส่วนใหญ่จะมีสีขาวหรือเทาอ่อน และเกิดขึ้นในบรรยากาศที่คงที่ ไม่มีการเปลี่ยนแปลงมากนัก เป็นเมฆซึ่งไม่ทำให้เกิดฝน แต่ส่วนใหญ่จะเป็นเกล็ดน้ำแข็ง' ,
      image : "assets/imgs/cotton.svg",
      image1 : "assets/imgs/cloud3.png"
    },
    {
      title : ' เมฆระดับกลาง อัลโต(Alto)',
      description : 'จะอยู่ที่ความสูงระหว่าง 2,000 – 6,000 เมตร  มีส่วนประกอบหลักคือ หยดน้ำ และมีน้ำแข็งบางส่วน แสงอาทิตย์ส่องผ่านเมฆชนิดนี้ได้' ,
      image : "assets/imgs/storm.svg",
      image1 : "assets/imgs/cloud4.png"
    },
  ]
}
